#!/bin/bash

export vblank_mode=0
export MESA_DEBUG=silent

git --no-pager log --oneline HEAD^..
step=$(git log --oneline origin/master.. | wc -l)

SHA=$(git log --oneline HEAD^.. | cut -f1 -d' ')
PREVSHA=$(git log --oneline HEAD^^..HEAD^ | cut -f1 -d' ')

cd /home/idr/devel/graphics/Mesa/BUILD

if [ "x$1" = "x" ]; then
    base="master"
else
    base=$1
fi

#for a in linux64 linux32; do
for a in linux64; do
    date
    echo Building $a...
#    $a /home/idr/devel/graphics/Mesa/build_with_meson.sh -b $base -d i965 -m fedora -v -D $SHA >/dev/null 2>&1
#    $a /home/idr/devel/graphics/Mesa/build_with_meson.sh -b $base -d i965 -v -D $SHA -c >/dev/null 2>&1

#    $a /home/idr/devel/graphics/Mesa/build_with_meson.sh -b $base -G -d auto -v -D debug >/dev/null 2>&1
#    if [ $? -ne 0 ]; then
#        echo Build fail
#        exit 125
#    fi

#    $a /home/idr/devel/graphics/Mesa/build_with_meson.sh -b $base -d i965 -m native -r -v -D rebase >/dev/null 2>&1
    $a /home/idr/devel/graphics/Mesa/build_with_meson.sh -b $base -d i965 -m native -r -v -D $SHA >/dev/null 2>&1
    if [ $? -ne 0 ]; then
        echo Build fail
        exit 125
    fi
done

if [ -d $SHA-64/lib64/dri -a -d $SHA-32/lib/dri ]; then
    size $SHA-64/lib64/dri/i965_dri.so $SHA-32/lib/dri/i965_dri.so
elif [ -d $SHA-64/lib64/dri/ ]; then
    size $SHA-64/lib64/dri/i965_dri.so
elif [ -d $SHA-32/lib/dri ]; then
    size $SHA-32/lib/dri/i965_dri.so
elif [ -d $SHA-64/lib/x86_64-linux-gnu/dri ]; then
    size $SHA-64/lib/x86_64-linux-gnu/dri/i965_dri.so
else
    exit 255
fi
#exit 0

export LD_LIBRARY_PATH=/opt/xorg-master-x86_64/lib64
export LIBGL_DRIVERS_PATH=/opt/xorg-master-x86_64/lib64/dri
export EGL_DRIVERS_PATH=/opt/xorg-master-x86_64/lib64/dri

CMD=$(basename $0)

if [ "x$CMD" = "xrebase_shader-db.sh" ]; then
    date
    echo shader-db...
    cd /home/idr/devel/graphics/shader-db
    #mesa $SHA ./run shaders > $(printf "results-rebase-%02d.txt" $step) 2> /dev/null
    PLAT=(ICL-Ice_Lake SKL-Skylake BDW-Broadwell HSW-Haswell IVB-Ivy_Bridge SNB-Sandy_Bridge ILK-Iron_Lake G4X-GM45)
    #PLAT=(SKL-Skylake BDW-Broadwell HSW-Haswell IVB-Ivy_Bridge SNB-Sandy_Bridge ILK-Iron_Lake G4X-GM45)
    #PLAT=(ICL-Ice_Lake ILK-Iron_Lake G4X-GM45)

    #PLAT=(SKL-Skylake BDW-Broadwell HSW-Haswell IVB-Ivy_Bridge SNB-Sandy_Bridge)
    #PLAT=(SKL-Skylake BDW-Broadwell)
    #PLAT=(SKL-Skylake)
    #PLAT=(HSW-Haswell)
    #PLAT=(ILK-Iron_Lake G4X-GM45)

    if [ -c /dev/dri/renderD129 ]; then
	device=1
    else
	device=0
    fi

    for x in ${PLAT[*]}; do
	a=$(echo $x | cut -d- -f1)
	b=$(echo $x | cut -d- -f2)

	resultname=$(printf "results-%s-%02d-%s.txt" $a $step $SHA)
	prevresultname=$(printf "results-%s-%02d-%s.txt" $a $((step - 1)) $PREVSHA)
	echo $b | sed 's/_/ /'
	mesa $SHA nice -20 ./intel_run -p $a shaders/ 2>&1 > $resultname | egrep -v '^(SKIP: shaders|ATTENTION: default value of option |Mesa warning: couldn|^WARNING: i965 does not fully support|^Instability or lower performance might)'

	if [ -f $prevresultname ]; then
	    ./report.py -c -s $prevresultname $resultname
	fi
	echo
    done

    exit 0
fi

if [ "x$CMD" = "xrebase_massif.sh" ]; then
    cd /home/idr/devel/graphics/shader-db

    for f in shaders/closed/steam/dirt-showdown/676.shader_test \
	     shaders/closed/steam/deus-ex-mankind-divided/2890.shader_test \
	     shaders/closed/steam/deus-ex-mankind-divided/148.shader_test \
	     shaders/closed/gfxbench5/aztec-ruins/high/11.shader_test \
	     shaders/dolphin/ubershaders/210.shader_test \
	     /home/idr/devel/graphics/piglit/tests/shaders/mean-soft-fp64-using-uint64.shader_test; do
	suffix=$(echo $f | sed 's/.shader_test$//;s|/|_|g')
	for x in $(seq 3); do
	    while true; do
		typeset -i l
		l=$(cat /proc/loadavg | cut -f1 -d.)
		if [ $l -lt 4 ]; then
		    break
		fi

		sleep 5
	    done

	    mesa $SHA valgrind --tool=massif --trace-children=yes --massif-out-file=massif.$SHA-$suffix.$x \
		 ./intel_run -p SKL -j 1 $f &
	done
    done
    wait
fi

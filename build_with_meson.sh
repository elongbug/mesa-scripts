#!/bin/bash

function usage
{
    echo "$1 [-c] [-b branch] [-d \"driver,driver,..\"]"
}

function notify
{
#	echo $1 | festival --tts &
	if [ "x$DISPLAY" != "x" ]; then
		if which notify-send > /dev/null; then
	    		notify-send -t 600000 "$1"
		fi
	else
		echo $1
	fi
}

if [ -d /opt/idr/devel/graphics/Mesa/BUILD ]; then
    cd /opt/idr/devel/graphics/Mesa/BUILD
else
    cd ~/devel/graphics/Mesa/BUILD
fi

typeset -i do_clean=0
typeset BRANCH=master
typeset drivers
typeset prefix
typeset -i do_install=0

ARCH=$(uname -m)
if [ -d /opt/xorg-master-$ARCH ]; then
    prefix=/opt/xorg-master-$ARCH
elif [ -d /opt/xorg/xorg-master-$ARCH ]; then
    prefix=/opt/xorg/xorg-master-$ARCH
else
    prefix=''
fi

if grep -q Atom /proc/cpuinfo ; then
    drivers="i915"
else
    drivers="i965"
fi

OPT="-O0 -mno-omit-leaf-frame-pointer"
debug='-D b_ndebug=false'
ARCHES=''
gallium=""
mode="native"
vulkan=''
typeset -i reconfigure=0

while getopts "cb:d:D:fGim:O:p:Rrv" flag
do
    case $flag in
    c) do_clean=1;;
    b) BRANCH=$OPTARG;;
    d) drivers=$OPTARG;;
    D) base_dir=$OPTARG;;
    f) OPT='-Og -mno-omit-leaf-frame-pointer'; debug='-D b_ndebug=false';;
    G) gallium="auto";;
    i) do_install=1;;
    m) mode=$OPTARG;;
    O) OPT="$OPTARG";;
    p) prefix=$OPTARG;;
    R) reconfigure=1;;
    r) OPT='-O3 -momit-leaf-frame-pointer'; debug='-D b_ndebug=true';;
    v) vulkan="intel";;
    *) usage $0; exit 2;;
    esac
done

WARN="-Wall -Wextra -Wunsafe-loop-optimizations -Werror=format-security -Wno-sign-compare -Wimplicit-fallthrough=0"
if echo "$OPT" | grep -q -e '-O[^01g]'; then
    DBG="-ggdb3 -Wp,-D_FORTIFY_SOURCE=2"
else
    DBG="-ggdb3"
fi

if [ "x$debug" == "x" ]; then
    DBG="$DBG -DDEBUG"
fi
CFLAGS="-pipe $WARN $OPT $DBG"

if [ $ARCH = x86_64 ]; then
    ARCH_BITS=64
else
    ARCH_BITS=32
fi

# To build i915/i965 to run on multiple architectures:
#
# ARCH_CFLAGS='-m64 -mtune=generic -march=core2 -msahf -mcx16 \
#              --param l1-cache-line-size=64'
#              
# ARCH_CFLAGS='-m32 -mtune=generic -march=core2 -msahf -mcx16 \
#              --param l1-cache-line-size=64 -mfpmath=sse'
#
# To build i915/i965 to run on the build system only:
#
# ARCH_CFLAGS='-m64 -march=native'
#              
# ARCH_CFLAGS='-m32 -march=native -mfpmath=sse'

case $mode in
native)
	ARCH_CFLAGS="-m$ARCH_BITS -march=native"
	if [ $ARCH != x86_64 ]; then
	    ARCH_CFLAGS="$ARCH_CFLAGS -mfpmath=sse"
	fi
	;;
byt)
	ARCH_CFLAGS="-m$ARCH_BITS -march=silvermont"
	if [ $ARCH != x86_64 ]; then
	    ARCH_CFLAGS="$ARCH_CFLAGS -mfpmath=sse"
	fi
	;;
haswell|k8-sse3|amdfam10)
	ARCH_CFLAGS="-m$ARCH_BITS -march=$mode"
	if [ $ARCH != x86_64 ]; then
	    ARCH_CFLAGS="$ARCH_CFLAGS -mfpmath=sse"
	fi
	;;
generic)
	ARCH_CFLAGS="-m$ARCH_BITS -mtune=generic -march=nocona -msahf -mcx16 \
                     --param l1-cache-line-size=64"

	if [ $ARCH != x86_64 ]; then
	    ARCH_CFLAGS="$ARCH_CFLAGS -mfpmath=sse"
	fi
	;;
fedora)
	debug='-D b_ndebug=true'
	if gcc -v 2>&1 | grep -q 'gcc version 4.[0-7]'; then
	    PROTECT='-fstack-protector --param=ssp-buffer-size=4'
	else
	    PROTECT='-fstack-protector-strong --param=ssp-buffer-size=4'
	fi

	CFLAGS="-O2 -g -pipe $WARN -Wp,-D_FORTIFY_SOURCE=2 -fexceptions $PROTECT -grecord-gcc-switches"
	if [ $ARCH = x86_64 ]; then
	    ARCH_CFLAGS="-m64 -mtune=generic"
	else
	    ARCH_CFLAGS="-m32 -march=i686 -mtune=atom -fasynchronous-unwind-tables"
	fi
	;;
*)
	echo "Invalid mode $mode"
	exit 2
	;;
esac

CFLAGS="$CFLAGS $ARCH_CFLAGS -Wno-missing-field-initializers"
export CFLAGS

export BRANCH
export CXXFLAGS="$CFLAGS"
if [ "x$prefix" != "x" ]; then
    export ACLOCAL="aclocal -I $prefix/share/aclocal/"
fi

if [ "x$base_dir" = "x" ]; then
    base_dir=$BRANCH
fi

export base_dir

LOG=./mesa_build.$$.txt

build_dir=${base_dir}-${ARCH_BITS}.build
install_dir=${base_dir}-${ARCH_BITS}

if [ $ARCH_BITS -eq 32 ]; then
    LIB=lib
else
    LIB=lib64
fi

if [ -d ../SOURCE/$BRANCH ] ; then
    src_dir=../SOURCE/$BRANCH
elif [ -d ../SOURCE/Mesa-$BRANCH ] ; then
    src_dir=../SOURCE/Mesa-$BRANCH
else
    echo FAIL
    notify "No Mesa source $(hostname)"
    exit 2
fi

export GIT_DIR=$src_dir/.git

if [ "x$prefix" != "x" ]; then
    export PKG_CONFIG_PATH="$prefix/$LIB/pkgconfig:$prefix/share/pkgconfig"
fi

for xxx in 1; do
    if [ $do_clean -ne 0 ]; then
	echo cleaning $ARCH_BITS
	rm -rf $build_dir $install_dir
    fi

    if [ ! -d $build_dir -o ! -f $build_dir/build.ninja -o $reconfigure -ne 0 ]; then
	COMMON_OPTS="--prefix=$PWD/$install_dir \
		-D buildtype=plain \
		-D platforms=drm,x11 \
		-D dri-drivers=$drivers \
		-D gallium-drivers=$gallium \
		-D gallium-xvmc=false \
		-D gallium-omx=disabled \
		-D gallium-va=false \
		-D vulkan-drivers=$vulkan \
		-D glx=dri \
		-D egl=true \
		-D dri=true \
		-D texture-float=true \
		-D build-tests=true \
		-D shared-glapi=true \
		-D gles1=true \
		-D gles2=true \
		-D opengl=true \
		-D tools=intel \
		$debug"

	echo meson $COMMON_OPTS $src_dir $build_dir
	if [ $reconfigure -eq 0 ]; then
	    meson $COMMON_OPTS $src_dir $build_dir
	else
	    meson configure $COMMON_OPTS $build_dir
	fi
    fi

    # $build_dir may not exit before running meson (just above here), so we
    # can't just redirect to it at the beginning.
    rm -f $build_dir/mesa_build.txt
    ln $LOG $build_dir/mesa_build.txt
    rm $LOG

    echo building $ARCH_BITS
    ninja -j$(getconf _NPROCESSORS_ONLN) -C $build_dir 2>&1

    if [ $? -ne 0 ]; then
	echo FAIL
#	notify "Mesa compilation failed for arch $i on $(hostname)"
	exit 125
    fi

    ninja -C $build_dir install

    # compiler/test_eu_compact currently fails on 32-bit
    if [ -f $GIT_DIR/BISECT_START -o $ARCH_BITS -eq 32 ]; then
# -o true
	echo "Skipping \"make check\" during bisect."
    else
	echo testing $ARCH_BITS
	ninja -C $build_dir test 2>&1

	if [ $? -ne 0 ]; then
	    echo FAIL
#	    notify "Mesa compilation failed for arch $ARCH_BITS on $(hostname)"
	    exit 1
	fi
    fi

done 2>&1 | tee $LOG

if ! tail ${build_dir}/mesa_build.txt | egrep -q 'FAIL($|[^:])'
then
    echo PASS
#    notify "Mesa compilation succeeded on $(hostname)"
    exit 0
else
    exit 1
fi

num_warn=$(grep warning: ${build_dir}/mesa_build.txt | wc -l)
echo $num_warn warnings this build.

#!/bin/bash

cd /home/idr/devel/graphics/Mesa/BUILD
echo Building
/home/idr/devel/graphics/Mesa/build_with_meson.sh -b master -d i965 > /dev/null 2> /dev/null
if [ $? -ne 0 ]; then
    echo Build fail
    exit 125
    #exit 1
fi

echo
echo Testing

mesa master glxinfo > /dev/null 2> /dev/null
if [ $? -ne 0 ]; then
    echo FAIL
    exit 255
fi

echo GOOD
exit 0

LD_LIBRARY_PATH=/home/idr/devel/graphics/Mesa/BUILD/kill-nv04-64/lib64/ glxinfo > /dev/null 2> /dev/null
if [ $? -ne 0 ]; then
    echo BAD
    exit 1
else
    echo GOOD
    exit 0
fi


#cd /home/idr/devel/graphics/ESCTS-3.2.1.0-20160603-70c8ad0/cts
#export MESA_GLES_VERSION_OVERRIDE=3.2
#mesa master ./glcts --deqp-case=ES3-CTS.functional.shaders.constants.int_l_suffix_vertex 2> /tmp/stderr.txt
#if [ $? -ne 0 ]; then
#    cat /tmp/stderr.txt
#    if grep -q Assertion /tmp/stderr.txt; then
#	echo BAD
#	echo
#	exit 1
#    else
#	echo SKIP - unrelated failure
#	echo
#	exit 125
#    fi
#else
#    echo GOOD
#    echo
#    exit 0
#fi

#cd /home/idr/devel/graphics/ESCTS-1.1.4.0-20150327/conform
#mesa master conform/conform -1 mipgen.c > /tmp/bisect.$$
#cat /tmp/bisect.$$
#if grep -q 'NO tests failed at any path level' /tmp/bisect.$$ ; then
#    echo GOOD
#    echo
#    rm -f /tmp/bisect.$$
#    exit 0
#else
#    echo BAD
#    echo
#    rm -f /tmp/bisect.$$
#    exit 1
#fi
